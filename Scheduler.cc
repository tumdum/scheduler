#include "Scheduler.h"
#include <ucontext.h>
#include <vector>
#include <iostream>
#include <algorithm>

const size_t STACK_SIZE = 4096 * 5;

struct EndCoroutineError {
    EndCoroutineError() {}
};

struct Context {
    Context(Coroutine coro) : coroutine(coro), ucontext(nullptr) {
        stack.resize(STACK_SIZE);
    }

    void prepare(ucontext_t* return_ctx, void (*coro) (), Scheduler* s) {
        ucontext.reset(new ucontext_t);
        if (getcontext(ucontext.get()) == -1) {
            throw std::runtime_error("getcontext failed");
        }
        ucontext->uc_stack.ss_sp = stack.data();
        ucontext->uc_stack.ss_size = stack.size();
        ucontext->uc_link = return_ctx;
        makecontext(ucontext.get(), coro, 2, s, this);
    }
    Coroutine coroutine;
    std::unique_ptr<ucontext_t> ucontext;
    std::vector<char> stack;
    bool ended = false;
    bool shouldEnd = false;
    std::function<bool ()> condition;
};

struct Scheduler::Pimpl {
    Pimpl(Scheduler& scheduler) : scheduler_(scheduler) {}
    ~Pimpl();
    void add(Coroutine coro);
    void yield();
    void wait_for(std::function<bool ()> cond);
    void run();
private:
    void run_one_round();
    void run_one_context(Context* context);
    static void runner(Scheduler* s, Context* c);

    Scheduler& scheduler_;
    std::vector<Coroutine> waiting_;
    std::vector<std::unique_ptr<Context>> contexes_;
    ucontext_t main_uctx_;
    Context* running_ctx_;
    size_t blocked_ = 0;
};

Scheduler::Pimpl::~Pimpl() {
    for (auto& ctx : contexes_) {
        ctx->shouldEnd = true;
        run_one_context(ctx.get());
    }
}

void Scheduler::Pimpl::add(Coroutine coro) {
    waiting_.push_back(coro);
}

void Scheduler::Pimpl::yield() {
    swapcontext(running_ctx_->ucontext.get(), &main_uctx_);
    if (running_ctx_->shouldEnd) {
        throw EndCoroutineError{};
    }
}

void Scheduler::Pimpl::wait_for(std::function<bool ()> cond) {
    running_ctx_->condition = cond;
    yield();
}

void Scheduler::Pimpl::run() {
    blocked_ = 0;
    while (contexes_.size() > blocked_ || !waiting_.empty()) {
        run_one_round();
    }
    // One last run - maybe some blocked coroutins got ready during last run.
    run_one_round();
    std::cerr << "[SCHEDULER] Blocked: " << blocked_ << ", existing: "
        << contexes_.size() << ", waiting: " << waiting_.size() << std::endl;
}

void Scheduler::Pimpl::run_one_round() {
    for (auto coro : waiting_) {
        contexes_.emplace_back(new Context(coro));
    }
    waiting_.clear();
    
    blocked_ = 0;
    for (auto& c : contexes_) {
        run_one_context(c.get());
    }

    auto end = std::remove_if(contexes_.begin(), contexes_.end(),
        [](std::unique_ptr<Context>& c) { return c->ended; });
    contexes_.erase(end, contexes_.end());
}

void Scheduler::Pimpl::run_one_context(Context* context) {
    if (context->ucontext == nullptr) {
        context->prepare(&main_uctx_,
            (void (*) ()) &Scheduler::Pimpl::runner, &scheduler_);
    }
    if ((context->condition && !context->condition()) && !context->shouldEnd) {
        blocked_++;
        return;
    }
    context->condition = std::function<bool ()>{};
    running_ctx_ = context;
    swapcontext(&main_uctx_, running_ctx_->ucontext.get());
}

void Scheduler::Pimpl::runner(Scheduler* s, Context* c) {
    try {
        c->coroutine(*s);
    } catch (EndCoroutineError&) {
    }
    c->ended = true;
}

Scheduler::Scheduler() : pimpl_(new Scheduler::Pimpl(*this)) {
}

Scheduler::~Scheduler() {
}

void Scheduler::add(Coroutine coro) {
    pimpl_->add(coro);
}

void Scheduler::yield() {
    pimpl_->yield();
}

void Scheduler::wait_for(std::function<bool ()> cond) {
    pimpl_->wait_for(cond);
}

void Scheduler::run() {
    pimpl_->run();
}
