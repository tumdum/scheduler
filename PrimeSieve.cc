#include "Scheduler.h"
#include "Channel.h"
#include <iostream>

/*
 * Port of 'Concurrent Prime Sieve' from golang.org page.
 */

using std::placeholders::_1;

void generate(Scheduler& , ChannelRef<int> ch) {
    for (int i = 2; ; i++) {
        ch->send(i);
    }
}

void filter(Scheduler& , ChannelRef<int> in, ChannelRef<int> out, int prime) {
    for (;;) {
        auto i = in->receive();
        if (i%prime != 0) {
            out->send(i);
        }
    }
}

void runner(Scheduler& s) {
    auto ch = Channel<int>::make(s, 1);
    s.add(std::bind(&generate, _1, ch));
    for (int i = 0; i < 10; i++) {
        auto prime = ch->receive();
        std::cout << prime << std::endl;
        auto ch1 = Channel<int>::make(s, 1);
        s.add(std::bind(&filter, _1, ch, ch1, prime));
        ch = ch1;
    }
}

int main() {
    Scheduler s;
    s.add(&runner);
    s.run();
}
