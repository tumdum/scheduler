#include <iostream>
#include "Scheduler.h"

int COUNTER = 1;

void coro1(Scheduler& s) {
    std::cout << __PRETTY_FUNCTION__ << ": 1" << std::endl;
    s.yield();
    COUNTER++;
    std::cout << __PRETTY_FUNCTION__ << ": END" << std::endl;
}

void coro2(Scheduler& s) {
    std::cout << __PRETTY_FUNCTION__ << ": 1" << std::endl;
    s.wait_for([]{ return COUNTER > 5; });
    std::cout << __PRETTY_FUNCTION__ << ": END -> COUNTER > 5: " << COUNTER << std::endl;
    s.add([](Scheduler& s) {
        std::cout << __PRETTY_FUNCTION__ << ": 1" << std::endl;
        s.wait_for([]{ return COUNTER > 100; });
        std::cout << __PRETTY_FUNCTION__ << ": 2 - NEVER" << std::endl;
    });
}

void coro3(Scheduler& s) {
    std::cout << __PRETTY_FUNCTION__ << ": 1" << std::endl;
    s.yield();
    COUNTER++;
    std::cout << __PRETTY_FUNCTION__ << ": 2" << std::endl;
    s.yield();
    COUNTER++;
    std::cout << __PRETTY_FUNCTION__ << ": END" << std::endl;
}

void coro4(Scheduler& s, const std::string& name) {
    std::cout << __PRETTY_FUNCTION__ << ": 1 " << name << std::endl;
    s.yield();
    COUNTER++;
    std::cout << __PRETTY_FUNCTION__ << ": 2 " << name << std::endl;
    s.yield();
    COUNTER++;
    std::cout << __PRETTY_FUNCTION__ << ": END " << name << std::endl;
}

int main() {
    Scheduler scheduler;
    scheduler.add(&coro1);
    scheduler.add(&coro2);
    scheduler.add(&coro3);
    scheduler.add(std::bind(&coro4, std::placeholders::_1, "TEST"));
    scheduler.add([](Scheduler& s) {
        std::cout << __PRETTY_FUNCTION__ << ": 1" << std::endl;
        s.yield();
        std::cout << __PRETTY_FUNCTION__ << ": END" << std::endl;
    });

    scheduler.run();
    return 0;
}
