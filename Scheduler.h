#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include <functional>
#include <memory>

struct Scheduler;
using Coroutine = std::function<void (Scheduler&)>;

struct Scheduler {
    Scheduler();
    ~Scheduler();

    Scheduler(const Scheduler&) = delete;
    Scheduler& operator=(const Scheduler&) = delete;

    void add(Coroutine coro);
    void yield();
    void wait_for(std::function<bool ()> cond);
    void run();
private:
    struct Pimpl;
    std::unique_ptr<Pimpl> pimpl_;
};

#endif  // SCHEDULER_H_
