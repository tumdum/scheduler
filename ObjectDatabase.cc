#include "Scheduler.h"
#include "Channel.h"
#include <unordered_map>
#include <unordered_set>
#include <iostream>

using Object = std::unordered_map<std::string, std::string>;
using DistName = std::string;
using DistNames = std::unordered_set<DistName>;
using Storage = std::unordered_map<DistName, Object>;

struct ExternalUpdate {
    Storage created;
    Storage updated;
    DistNames deleted;
};

std::ostream& operator<<(std::ostream& os, const ExternalUpdate& eu) {
    os << "ExternalUpdate{ created: {";
    for (const auto& created : eu.created) {
        os << created.first << ",";
    }
    os << "}}";
    return os;
}

struct ObjectDatabase;

struct Subscription {
    ~Subscription();
    ExternalUpdate wait();
private:
    friend class ObjectDatabase;
    Subscription(const DistNames& names, ChannelRef<ExternalUpdate> channel,
        ObjectDatabase& db) 
    : db_(db), names_(names), channel_(channel) {
    }
    ObjectDatabase& db_;
    const DistNames names_;
    ChannelRef<ExternalUpdate> channel_;
};

struct ObjectDatabase {
    ObjectDatabase(Scheduler& s) : scheduler_{s} {}

    void handle(const ExternalUpdate& update) {
        std::unordered_set<ChannelRef<ExternalUpdate>> toSignal;
        for (const auto& pair : update.created) {
            auto range = subscriptions_.equal_range(pair.first);
            for (auto it = range.first; it != range.second; ++it) {
                toSignal.insert(it->second);
            }
        }
        for (const auto& pair : update.updated) {
            auto range = subscriptions_.equal_range(pair.first);
            for (auto it = range.first; it != range.second; ++it) {
                toSignal.insert(it->second);
            }
        }
        for (auto ch : toSignal) {
            ch->try_send(update);
        }
    }

    Subscription subscribe(const DistNames& names) {
        auto ch = Channel<ExternalUpdate>::make(scheduler_, 1);
        for (const auto& name : names) {
            subscriptions_.insert({name, ch});
        }
        return Subscription{names, ch, *this};
    }
private:
    friend class Subscription;
    void unsubscribe(const DistNames&, ChannelRef<ExternalUpdate> ) {
        // TODO(klak) :)
    }
    using Subscriptions = std::unordered_multimap<DistName, ChannelRef<ExternalUpdate>>;
    Subscriptions subscriptions_;
    Storage objects_;
    Scheduler& scheduler_;

    ExternalUpdate lastUpdated_;
};

Subscription::~Subscription() {
    db_.unsubscribe(names_, channel_);
}

ExternalUpdate Subscription::wait() {
    return channel_->receive();
}

bool bHasCorrectParameterValue(Subscription s) {
    auto update = s.wait();
    return update.created["B"]["parameter"] == "expected" ||
        update.updated["B"]["parameter"] == "expected";
}

void abc(Scheduler&, ObjectDatabase& db) {
    auto sa = db.subscribe({"A"});
    auto sb = db.subscribe({"B"});
    std::cout << __PRETTY_FUNCTION__ << ": start" << std::endl;
    auto a = sa.wait();
    std::cout << __PRETTY_FUNCTION__ << ": " << a << std::endl;
    while(!bHasCorrectParameterValue(sb));
    std::cout << __PRETTY_FUNCTION__ << "B has correct value!" << std::endl;
    const std::string old = "expected";
    while (sb.wait().updated["B"]["parameter"] == old);
    std::cout << __PRETTY_FUNCTION__ << "B has changed value!" << std::endl;

}

void tick(const std::string& faze_name,
    const std::string& object_name,
    const Object& object,
    bool is_update,
    ObjectDatabase& db,
    Scheduler& s)
{
    std::cout << ">>> " << faze_name << std::endl;
    ExternalUpdate update;
    if (is_update) {
        update.updated.insert({object_name, object});
    } else {
        update.created.insert({object_name, object});
    }
    db.handle(update);
    s.run();
}

int main() {
    Scheduler s;
    ObjectDatabase db{s};
    s.add(std::bind(&abc, std::placeholders::_1, std::ref(db)));
    std::cout << __PRETTY_FUNCTION__ << "initial run" << std::endl;
    s.run();

    tick("run after A created", "A", Object{}, false, db, s);

    tick("run with B byt incorrect value", "B", 
        {{"parameter", "icorrect"}}, false, db, s);

    tick("run with B with expected value", "B",
        {{"parameter", "expected"}}, true, db, s);

    tick("again run with B with expected value", "B",
        {{"parameter", "expected"}}, true, db, s);

    tick("run with changed value", "B",
        {{"parameter", "different"}}, true, db, s);

    std::cout << __PRETTY_FUNCTION__ << "end" << std::endl;
}
