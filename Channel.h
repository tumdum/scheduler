#ifndef CHANNEL_H_
#define CHANNEL_H_

#include <queue>
#include <cassert>
#include <memory>
#include "Scheduler.h"

template <typename T>
struct Channel;

template <typename T>
using ChannelRef = std::shared_ptr<Channel<T>>;

template <typename T>
struct Channel : std::enable_shared_from_this<Channel<T>> {
    static ChannelRef<T> make(Scheduler& s, size_t max = 0);
    void send(const T& t);
    bool try_send(const T& t);
    T receive();
private:
    Channel(Scheduler& s, size_t max);
    bool canSend() const;
    std::queue<T> queue_;
    Scheduler& scheduler_;
    size_t max_size_;
    bool end_coroutine = false;
};

template <typename T>
ChannelRef<T> Channel<T>::make(Scheduler& s, size_t max) {
    return ChannelRef<T>{new Channel<T>{s, max}};
}

template <typename T>
Channel<T>::Channel(Scheduler& s, size_t max) : scheduler_(s), max_size_(max) {
}

template <typename T>
void Channel<T>::send(const T& t) {
    if (!canSend()) {
        auto this_sp = this->shared_from_this();
        scheduler_.wait_for([=] { return this_sp->queue_.size() < max_size_; });
    }
    queue_.push(t);
    scheduler_.yield();
}

template <typename T>
bool Channel<T>::try_send(const T& t) {
    if (!canSend()) {
        return false;
    }
    queue_.push(t);
    return true;
}

template <typename T>
T Channel<T>::receive() {
    if (queue_.empty()) {
        auto this_sp = this->shared_from_this();
        scheduler_.wait_for([this_sp] { return !this_sp->queue_.empty(); });
    }
    const T top = queue_.front();
    queue_.pop();
    scheduler_.yield();
    return top;
}

template <typename T>
bool Channel<T>::canSend() const {
    return max_size_ == 0 || queue_.size() < max_size_;
}

#endif  // CHANNEL_H_
