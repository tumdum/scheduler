#include "Channel.h"
#include "Scheduler.h"
#include <cassert>
#include <iostream>

void coro(Scheduler& s) {
    auto c = Channel<int>::make(s);
    c->send(42);
    assert(42 == c->receive());
}

void coro_consumer(Scheduler& , ChannelRef<int> inputs) {
    for (int i = 0; i < 6; ++i) {
        std::cout << inputs->receive() << std::endl;
    }
}

void coro_producer(Scheduler& , ChannelRef<int> output) {
    for (int i : {1, 2, 3, 4, 42, 100}) {
        output->send(i);
    }
}

void coro_subcorochannel(Scheduler& s) {
    auto c = Channel<int>::make(s);
    s.add([c] (Scheduler&) mutable {
        for (int i = 0; i != 3; ++i) {
            std::cout << __PRETTY_FUNCTION__ << ": " << c->receive()
                << " NEVER" << std::endl;
        }
    });

}

int main() {
    Scheduler s;
    s.add(&coro);
    auto c = Channel<int>::make(s);
    s.add(std::bind(&coro_consumer, std::placeholders::_1, c));
    s.add(std::bind(&coro_producer, std::placeholders::_1, c));
    s.add(&coro_subcorochannel);
    s.run();
}
