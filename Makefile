CXX=g++ -std=c++11 -Wall -Wextra -pedantic -g

Scheduler.o: Scheduler.cc Scheduler.h

example1: example1.o Scheduler.o
	$(CXX) example1.o Scheduler.o -o example1

example2: example2.o Scheduler.o Channel.h
	$(CXX) example2.o Scheduler.o -o example2

prime_sieve: PrimeSieve.o Scheduler.o Channel.h
	$(CXX) PrimeSieve.o Scheduler.o -o prime_sieve

object_database: ObjectDatabase.o Scheduler.o Channel.h
	$(CXX) ObjectDatabase.o Scheduler.o -o object_database

clean:
	rm -f *.o
	rm -f example1 example2
